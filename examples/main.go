package main

import (
	"fmt"
	"gitlab.com/jensdewaard/namegenerator"
)

func main() {
	fmt.Printf("%v\n", namegenerator.Generate())
}
