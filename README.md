This code generates human-readable names in the pattern 
<adjective>-<noun>-<adjective>-<noun>. We needed 'unique' names for our devices
so we had some fun with it. 