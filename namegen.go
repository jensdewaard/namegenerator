package namegenerator

import (
	"math/rand"
	"strings"
	"time"
)

var adjectiveList = []string{
	"average",
	"breezy",
	"calculated",
	"cheerful",
	"cold",
	"cool",
	"crouching",
	"doubting",
	"eager",
	"epic",
	"fluffy",
	"frantic",
	"gentle",
	"handsome",
	"helpful",
	"hidden",
	"kind",
	"lively",
	"nonfunctional",
	"obedient",
	"rhyming",
	"striped",
	"victorious",
	"walking",
	"warm",
	"welcoming",
	"wide-eyed",
	"witty",
}

var nounList = []string{
	"afternoon",
	"bat",
	"cactus",
	"chaos",
	"cherries",
	"crown",
	"doggo",
	"doll",
	"dragon",
	"frame",
	"home",
	"jellyfish",
	"johnny",
	"kitten",
	"ladybug",
	"lettuce",
	"locket",
	"mark",
	"mountain",
	"pickle",
	"pirate",
	"reason",
	"sandwich",
	"telephone",
	"tiger",
	"toaster",
	"turkey",
	"volcano",
	"voyage",
	"wanderer",
}

func Generate() string {
	rand.Seed(time.Now().UnixNano())
	var words [4]string
	words[0] = adjectiveList[rand.Intn(len(adjectiveList))]
	words[1] = nounList[rand.Intn(len(nounList))]
	words[2] = adjectiveList[rand.Intn(len(adjectiveList))]
	words[3] = nounList[rand.Intn(len(nounList))]
	return strings.Join(words[:], "-")
}
